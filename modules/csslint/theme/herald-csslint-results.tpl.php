<?php

/**
 * @file
 * Drupal CSSLint task result rendering.
 *
 * Available variables:
 * - $file_stats: An array of statistics, per file.
 * - $results: Array of results.
 * - $task: The task node.
 */
?>
<div class="csslint-results">
  <?php if (!empty($results)): ?>
    <div class="csslint-results__grade">
      <h3><?php print t("Grade for this build: !grade", array('!grade' => call_user_func_array('herald_csslint_compute_grade', $file_stats))); ?></h3>
    </div>
  <?php endif; ?>

  <div class="csslint-results__stats">
    <h3><?php print t("Statistics"); ?></h3>

    <?php print theme('table', array(
      'header' => array(t("Total number of files"), t("Files without errors"), t("Files with errors")),
      'rows' => array($file_stats),
    )); ?>
  </div>

  <?php if (!empty($results)): ?>
    <div class="csslint-results__details">
      <h3><?php print t("Details"); ?></h3>

      <?php foreach ($results as $result): ?>
        <?php print $result; ?>
      <?php endforeach; ?>
    </div>
  <?php else: ?>
    <div class="csslint-results__no-issues">
      <h3><?php print t("The build did not contain any CSS files."); ?></h3>
    </div>
  <?php endif; ?>
</div>
