<?php

/**
 * @file
 * Template for rendering a single task run result.
 *
 * Available variables:
 * - $file: The filename for this result, safe for printing.
 * - $result: The result.
 */
?>
<div class="csslint-results__details__result">
  <h4 class="csslint-results__details__result__file-name csslint-results__details__result__file-name--<?php print !empty($error) ? 'error' : 'ok'; ?>">
    <?php print $file; ?>
  </h4>

  <?php if (!empty($result)): ?>
    <div class="csslint-results__details__result__error">
      <pre><code>
        <?php print check_plain($result); ?>
      </code></pre>
    </div>
  <?php endif; ?>
</div>
