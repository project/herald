/**
 * @file
 * Make some enhancements to the Herald Project node form.
 */

(function($, Drupal) {
  'use strict';

  Drupal.behaviors.heraldSimpletestHeraldProjectNode = {

    attach: function(context, settings) {
      var $taskTypes = $('input[name^="herald_task_types[und][simpletest"]', context);
      if ($taskTypes.length) {
        var $coreRadio = $('input[name="herald_project_core_version[und]"]:not(.js-processed)', context);

        if ($coreRadio.length) {
          var hideImages = function(core) {
            $taskTypes.each(function() {
              var $taskType = $(this),
                  value = $taskType.val().replace('simpletest_', '');

              // Is this compatible with the selected version of core?
              if (settings.heraldSimpletest.images[value].core === core) {
                // Re-enable it, if necessary.
                $taskType.attr('disabled', false);

                // Was it checked before? If so, check it again.
                if ($taskType.hasClass('js-herald-simpletest-was-checked')) {
                  $taskType.attr('checked', true).removeClass('js-herald-simpletest-was-checked');
                }
              }
              else {
                // Disable it.
                $taskType.attr('disabled', true);

                // Was it checked? If so, add our flag.
                if ($taskType.is(':checked')) {
                  $taskType.attr('checked', false).addClass('js-herald-simpletest-was-checked');
                }
              }
            });
          };

          $coreRadio.change(function() {
            var core = $('input[name="herald_project_core_version[und]"]:checked').val();
            hideImages(core);
          });

          $coreRadio.addClass('js-processed');
        }
      }
    }

  };

})(jQuery, Drupal);
