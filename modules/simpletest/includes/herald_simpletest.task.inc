<?php

/**
 * @file
 * Logic for running Herald tasks.
 */

/**
 * Implements hook_herald_task_view().
 */
function _herald_simpletest_herald_task_view($task_type, $task) {
  if (preg_match('/^simpletest_/', $task_type)) {
    $results = herald_simpletest_get_result($task);

    $tests_stats = array(
      'num_assertions' => 0,
      'num_passes' => 0,
      'num_fails' => 0,
      'num_exceptions' => 0,
    );

    $rendered_results = array();
    $test_log = array();
    $test_stats = array(
      'num_assertions' => 0,
      'num_passes' => 0,
      'num_fails' => 0,
      'num_exceptions' => 0,
    );
    if (!empty($results)) {
      foreach ($results as $line) {
        $line = trim(preg_replace('/\[\w+\]/', '', $line));
        // Is this line a "Starting test" message?
        if (preg_match('/Starting test [\w_]+/', $line)) {
          // Did we already have a log? If so, store it now and start again.
          if (!empty($test_log)) {
            $rendered_results[] = theme('herald_simpletest_result', array(
              'task' => $task,
              'test_log' => $test_log,
              'test_stats' => $test_stats,
            ));
            $test_log = array();
            $test_stats = array(
              'num_assertions' => 0,
              'num_passes' => 0,
              'num_fails' => 0,
              'num_exceptions' => 0,
            );
          }
        }
        elseif (preg_match('/\d+ passes, \d+ fails/', $line)) {
          // Fetch the stats.
          $match;
          preg_match('/(\d+) passes/', $line, $match);
          $tests_stats['num_assertions'] += $match[1];
          $tests_stats['num_passes'] += $test_stats['num_passes'] = $match[1];

          preg_match('/(\d+) fails/', $line, $match);
          $tests_stats['num_assertions'] += $match[1];
          $tests_stats['num_fails'] += $test_stats['num_fails'] = $match[1];

          preg_match('/(\d+) exceptions/', $line, $match);
          $tests_stats['num_assertions'] += $match[1];
          $tests_stats['num_exceptions'] += $test_stats['num_exceptions'] = $match[1];
        }

        $test_log[] = $line;
      }

      // Get the last one as well.
      $rendered_results[] = theme('herald_simpletest_result', array(
        'task' => $task,
        'test_log' => $test_log,
        'test_stats' => $test_stats,
      ));
    }

    return array(
      'herald_simpletest' => array(
        '#theme' => 'herald_simpletest_results',
        '#test_stats' => $tests_stats,
        '#results' => $rendered_results,
        '#task' => $task,
      ),
    );
  }
}

/**
 * Implements hook_herald_run_task().
 */
function _herald_simpletest_herald_run_task($task_type, $task, $build, $project) {
  if (preg_match('/^simpletest_/', $task_type)) {
    // Get the build code location.
    $build_location = herald_get_build_location($build);
    $settings = herald_simpletest_get_settings($project);

    // Get the image settings.
    $image = preg_replace('/^simpletest_/', '', $task_type);
    $images = herald_simpletest_get_docker_images();
    $image_settings = $images[$image];

    // Run the tests.
    $results = herald_simpletest_run_test(
      $settings['prerun_commands'],
      $build_location,
      $image_settings['www_path'],
      $settings['run_tests'],
      $image
    );

    // Store the results.
    herald_simpletest_store_result($task, $results);

    // Mark the test as terminated.
    herald_task_terminated($task);
  }
}

/**
 * Run the task.
 *
 * Runs the unit tests inside a Docker container, which will only exist for as
 * long as the test run. It will then be removed immediately.
 *
 * @param string $prerun_commands
 *    A list of commands to run before executing the tests. One command per
 *    line.
 * @param string $build_location
 *    The location of the build code.
 * @param string $www_location
 *    The location of the Drupal code inside the container.
 * @param string $tests
 *    A comma-separated list of tests and/or groups, or --all.
 * @param string $image
 *    The name of the image to run.
 *
 * @return string
 *    The test results.
 */
function herald_simpletest_run_test($prerun_commands, $build_location, $www_location, $tests, $image) {
  $results = array();
  $status;
  $command = "docker run -it \
-v {$build_location}:{$www_location}/sites/all/modules/herald_simpletest_temp_build \
--rm {$image} \
sh -c '\\\n";

  $prerun_commands = explode("\n", str_replace(array("\r\n", "\r"), "\n", $prerun_commands));

  if (!empty($prerun_commands)) {
    foreach ($prerun_commands as $prerun_command) {
      if (empty($prerun_command)) {
        continue;
      }

      $prerun_command = str_replace(
        array('{www_location}'),
        array($www_location),
        $prerun_command
      );
      $command .= str_replace("'", "\\'", $prerun_command) . " && \\\n";
    }
  }

  $command .= "supervisord  > /dev/null 2>&1 && \
sleep 5 > /dev/null 2>&1 && \
drush --root={$www_location} en simpletest -y > /dev/null 2>&1 && \
drush --root={$www_location} cc registry > /dev/null 2>&1 && \
drush --root={$www_location} --uri=http://localhost --nocolor test-run {$tests}'";

  exec($command, $results, $status);

  watchdog('herald_simpletest', "Results for running tests '%tests' (command: '%command'): %results", array(
    '%command' => $command,
    '%tests' => $tests,
    '%results' => print_r($results, 1),
  ), WATCHDOG_INFO);

  return $results;
}
