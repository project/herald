<?php

/**
 * @file
 * Docker specific logic.
 */

/**
 * Re-build a Docker image.
 *
 * @todo Check if the image really got built.
 *
 * @param string $image
 *    The image name that will be built.
 * @param string $dockerfile_location
 *    The location of the Dockerfile.
 *
 * @return bool
 *    TRUE on success, FALSE otherwise.
 */
function herald_simpletest_rebuild_docker_image($image, $dockerfile_location) {
  if (!file_exists("{$dockerfile_location}/Dockerfile")) {
    watchdog(
      'herald_simpletest',
      "Couldn't find Dockerfile in @location. Aborting build of @image.",
      array('@location' => $dockerfile_location, '@image' => $image),
      WATCHDOG_ERROR
    );
    return FALSE;
  }

  return preg_match('/Sending build context/', shell_exec("cd {$dockerfile_location} && docker build -t {$image} ."));
}
