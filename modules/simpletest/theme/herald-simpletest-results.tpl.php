<?php

/**
 * @file
 * Drupal Simpletest task result rendering.
 *
 * Available variables:
 * - $test_stats: An array of statistics, globally.
 * - $results: Array of results, already rendered and safe for printing.
 * - $task: The task node.
 */
?>
<div class="herald-simpletest-results">
  <?php if (!empty($test_stats['num_assertions'])): ?>
    <div class="herald-simpletest-results__grade">
      <h3><?php print t("Grade for this build: !grade", array('!grade' => call_user_func_array('herald_simpletest_compute_grade', $test_stats))); ?></h3>
    </div>
  <?php endif; ?>

  <div class="herald-simpletest-results__stats">
    <h3><?php print t("Statistics"); ?></h3>

    <?php print theme('table', array(
      'header' => array(t("Total number of assertions"), t("Passes"), t("Fails"), t("Exceptions")),
      'rows' => array($test_stats),
    )); ?>
  </div>

  <?php if (!empty($results)): ?>
    <div class="herald-simpletest-results__details">
      <h3><?php print t("Details"); ?></h3>

      <?php foreach ($results as $result): ?>
        <?php print $result; ?>
      <?php endforeach; ?>
    </div>
  <?php else: ?>
    <div class="herald-simpletest-results__no-results">
      <h3><?php print t("No results to show."); ?></h3>
    </div>
  <?php endif; ?>
</div>
