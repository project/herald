<?php

/**
 * @file
 * Template for rendering a single task run result.
 *
 * Available variables:
 * - $test_log: An array of lines returned by the test runner.
 * - $test_name: The name of the test.
 * - $test_stats: Test stats.
 * - $task: The task node.
 */
$error_type =
  !empty($test_stats['num_fails']) ? 'failed' :
    (!empty($test_stats['num_exceptions']) ? 'exceptions' : 'passed');
?>
<div class="herald-simpletest-results__details__result">
  <h4 class="herald-simpletest-results__details__result__test-name herald-simpletest-results__details__result__test-name--<?php print $error_type; ?>">
    <?php print check_plain($test_name); ?>
  </h4>

  <div class="herald-simpletest-results__details__result__log">
    <pre><code>
      <?php print implode("\n", array_map(function($item) {
        return check_plain($item);
      }, $test_log)); ?>
    </code></pre>
  </div>
</div>
