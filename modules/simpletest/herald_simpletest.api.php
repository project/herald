<?php

/**
 * @file
 * Hook documentation.
 */

/**
 * Implements hook_herald_simpletest_docker_image().
 *
 * Return a list of available Docker images, represented by a Dockerfile.
 * Each image should already contain a full Drupal install so we can run
 * tests against it, as well as having Drush in the users' PATH.
 *
 * @return array
 *    An array of available Dockerfiles, keyed by image name. Each entry must
 *    provide the following information:
 *    - title: The name of the image. Be as specific as possible, like
 *      providing PHP version info, what webserver, etc.
 *    - dockerfile_location: An absolute path to the location of the Dockerfile
 *      to build
 *    - core: With which version of core the image is compatible. Can be 6.x,
 *      7.x, 8.x, etc.
 *    - www_path: Where the Drupal site is installed.
 */
function herald_simpletest_herald_simpletest_docker_image() {
  $path = realpath(drupal_get_path('module', 'herald_simpletest'));

  return array(
    'herald/php54-mysql55-apache22' => array(
      'title' => t("PHP 5.4, MySQL 5.5 and Apache 2.2"),
      'dockerfile_location' => "$path/docker/php5.4_mysql5.5_apache2.2/",
      'core' => '7.x',
      'www_path' => '/var/www',
    ),
  );
}
