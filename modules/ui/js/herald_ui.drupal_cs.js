/**
 * @file
 * Charts for drupal_cs tasks.
 */

(function($, Drupal) {
  'use strict';

  Drupal.behaviors.heraldUIDrupalCS = {

    attach: function(context, settings) {
      var $ctx = $('.herald-ui-drupal-cs.herald-ui-results__stats canvas:not(.js-processed)', context);

      if ($ctx.length) {
        $ctx.each(function() {
          var ctx = this.getContext('2d'),
              $this = $(this),
              fileStats = JSON.parse(
                // This is a trick to remove HTML entities and treat them as
                // normal chars.
                $('<textarea />').html($this.attr('data-file-stats')).text()
              );

          // Prepare the data.
          var data = [
            {
              value: fileStats.num_ok,
              color: settings.herald_ui.colors.green.normal,
              highlight: settings.herald_ui.colors.green.highlight,
              label: Drupal.t("Ok")
            },
            {
              value: fileStats.num_minor,
              color: settings.herald_ui.colors.blue.normal,
              highlight: settings.herald_ui.colors.blue.highlight,
              label: Drupal.t("Minor")
            },
            {
              value: fileStats.num_normal,
              color: settings.herald_ui.colors.orange.normal,
              highlight: settings.herald_ui.colors.orange.highlight,
              label: Drupal.t("Normal")
            },
            {
              value: fileStats.num_critical,
              color: settings.herald_ui.colors.red.normal,
              highlight: settings.herald_ui.colors.red.highlight,
              label: Drupal.t("Critical")
            }
          ];

          new Chart(ctx).Doughnut(data, {percentageInnerCutout: 75});

          $this.addClass('js-processed');
        });
      }
    }

  };

})(jQuery, Drupal);
