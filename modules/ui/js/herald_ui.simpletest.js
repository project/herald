/**
 * @file
 * Charts for simpletest tasks.
 */

(function($, Drupal) {
  'use strict';

  Drupal.behaviors.heraldUISimpletest = {

    attach: function(context, settings) {
      var $ctx = $('.herald-ui-simpletest.herald-ui-results__stats canvas:not(.js-processed)', context);

      if ($ctx.length) {
        $ctx.each(function() {
          var ctx = this.getContext('2d'),
              $this = $(this),
              testStats = JSON.parse(
                // This is a trick to remove HTML entities and treat them as
                // normal chars.
                $('<textarea />').html($this.attr('data-test-stats')).text()
              );

          // Prepare the data.
          var data = [
            {
              value: testStats.num_passes,
              color: settings.herald_ui.colors.green.normal,
              highlight: settings.herald_ui.colors.green.highlight,
              label: Drupal.t("Passes")
            },
            {
              value: testStats.num_exceptions,
              color: settings.herald_ui.colors.orange.normal,
              highlight: settings.herald_ui.colors.orange.highlight,
              label: Drupal.t("Exceptions")
            },
            {
              value: testStats.num_fails,
              color: settings.herald_ui.colors.red.normal,
              highlight: settings.herald_ui.colors.red.highlight,
              label: Drupal.t("Fails")
            }
          ];

          new Chart(ctx).Doughnut(data, {percentageInnerCutout: 75});

          $this.addClass('js-processed');
        });
      }
    }

  };

})(jQuery, Drupal);
