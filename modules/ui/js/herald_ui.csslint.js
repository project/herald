/**
 * @file
 * Charts for csslint tasks.
 */

(function($, Drupal) {
  'use strict';

  Drupal.behaviors.heraldUICSSLint = {

    attach: function(context, settings) {
      var $ctx = $('.herald-ui-csslint.herald-ui-results__stats canvas:not(.js-processed)', context);

      if ($ctx.length) {
        $ctx.each(function() {
          var ctx = this.getContext('2d'),
              $this = $(this),
              fileStats = JSON.parse(
                // This is a trick to remove HTML entities and treat them as
                // normal chars.
                $('<textarea />').html($this.attr('data-file-stats')).text()
              );

          // Prepare the data.
          var data = [
            {
              value: fileStats.num_ok,
              color: settings.herald_ui.colors.green.normal,
              highlight: settings.herald_ui.colors.green.highlight,
              label: Drupal.t("Ok")
            },
            {
              value: fileStats.num_error,
              color: settings.herald_ui.colors.red.normal,
              highlight: settings.herald_ui.colors.red.highlight,
              label: Drupal.t("Error")
            }
          ];

          new Chart(ctx).Doughnut(data, {percentageInnerCutout: 75});

          $this.addClass('js-processed');
        });
      }
    }

  };

})(jQuery, Drupal);
