<?php

/**
 * @file
 * Drupal Simpletest task result rendering.
 *
 * Available variables:
 * - $test_stats: An array of statistics, per file.
 * - $results: Array of results.
 * - $task: The task node.
 */

// Pre-compute the grade.
$grade = !empty($test_stats['num_assertions']) ? call_user_func_array('herald_simpletest_compute_grade', $test_stats) : NULL;
?>
<div class="herald-ui-simpletest herald-ui-results">
  <?php if (!empty($test_stats['num_assertions'])): ?>
    <div class="herald-ui-simpletest herald-ui-results__stats">
      <h3 class="herald-ui-simpletest herald-ui-results__stats__grade herald-ui-simpletest herald-ui-results__stats__grade--<?php print strtolower($grade); ?>"><?php print $grade; ?></h3>
      <canvas class="herald-ui-simpletest herald-ui-results__stats__chart"<?php print drupal_attributes(array('data-test-stats' => json_encode($test_stats))); ?>></canvas>
    </div>

    <?php print ctools_modal_text_button(t("View details"), "herald-ui/nojs/task/{$task->nid}", t("See task run details"), 'herald-ui-simpletest herald-ui-results__modal-link'); ?>
  <?php else: ?>
    <div class="herald-ui-simpletest herald-ui-results__no-results">
      <?php print t("No results."); ?>
    </div>
  <?php endif; ?>
</div>
