<?php

/**
 * @file
 * Drupal code standards task result rendering.
 *
 * Available variables:
 * - $stats: An array of statistics, globally.
 * - $file_stats: An array of statistics, per file.
 * - $results: Array of results, already rendered and safe for printing.
 * - $task: The task node.
 */

// Pre-compute the grade.
$grade = !empty($results) ? call_user_func_array('herald_drupal_cs_compute_grade', $file_stats) : NULL;
?>
<div class="herald-ui-drupal-cs herald-ui-results">
  <?php if (!empty($results)): ?>
    <div class="herald-ui-drupal-cs herald-ui-results__stats">
      <h3 class="herald-ui-drupal-cs herald-ui-results__stats__grade herald-ui-drupal-cs herald-ui-results__stats__grade--<?php print strtolower($grade); ?>"><?php print $grade; ?></h3>
      <canvas class="herald-ui-drupal-cs herald-ui-results__stats__chart"<?php print drupal_attributes(array('data-file-stats' => json_encode($file_stats))); ?>></canvas>
    </div>

    <?php print ctools_modal_text_button(t("View details"), "herald-ui/nojs/task/{$task->nid}", t("See task run details"), 'herald-ui-drupal-cs herald-ui-results__modal-link'); ?>
  <?php else: ?>
    <div class="herald-ui-csslint herald-ui-results__no-results">
      <?php print t("No results."); ?>
    </div>
  <?php endif; ?>
</div>
