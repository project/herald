<?php

/**
 * @file
 * Drupal CSSLint task result rendering.
 *
 * Available variables:
 * - $file_stats: An array of statistics, per file.
 * - $results: Array of results.
 * - $task: The task node.
 */

// Pre-compute the grade.
$grade = !empty($results) ? call_user_func_array('herald_csslint_compute_grade', $file_stats) : NULL;
?>
<div class="herald-ui-csslint herald-ui-results">
  <?php if (!empty($grade)): ?>
    <div class="herald-ui-csslint herald-ui-results__stats">
      <h3 class="herald-ui-csslint herald-ui-results__stats__grade herald-ui-csslint herald-ui-results__stats__grade--<?php print strtolower($grade); ?>"><?php print $grade; ?></h3>
      <canvas class="herald-ui-csslint herald-ui-results__stats__chart"<?php print drupal_attributes(array('data-file-stats' => json_encode($file_stats))); ?>></canvas>
    </div>

    <?php print ctools_modal_text_button(t("View details"), "herald-ui/nojs/task/{$task->nid}", t("See task run details"), 'herald-ui-csslint herald-ui-results__modal-link'); ?>
  <?php else: ?>
    <div class="herald-ui-csslint herald-ui-results__no-files">
      <?php print t("No CSS files found."); ?>
    </div>
  <?php endif; ?>
</div>
