<?php

/**
 * @file
 * Drupal ESLint task result rendering.
 *
 * Available variables:
 * - $file_stats: An array of statistics, per file.
 * - $results: Array of results.
 * - $task: The task node.
 */

// Pre-compute the grade.
$grade = !empty($results) ? call_user_func_array('herald_eslint_compute_grade', $file_stats) : NULL;
?>
<div class="herald-ui-eslint herald-ui-results">
  <?php if (!empty($results)): ?>
    <div class="herald-ui-eslint herald-ui-results__stats">
      <h3 class="herald-ui-eslint herald-ui-results__stats__grade herald-ui-eslint herald-ui-results__stats__grade--<?php print strtolower($grade); ?>"><?php print $grade; ?></h3>
      <canvas class="herald-ui-eslint herald-ui-results__stats__chart"<?php print drupal_attributes(array('data-file-stats' => json_encode($file_stats))); ?>></canvas>
    </div>

    <?php print ctools_modal_text_button(t("View details"), "herald-ui/nojs/task/{$task->nid}", t("See task run details"), 'herald-ui-eslint herald-ui-results__modal-link'); ?>
  <?php else: ?>
    <div class="herald-ui-csslint herald-ui-results__no-files">
      <?php print t("No JS files found."); ?>
    </div>
  <?php endif; ?>
</div>
