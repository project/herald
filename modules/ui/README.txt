Installation
============

Download Chart.js () and put it inside the site's libraries folder (for example, sites/all/libraries/). Unzip the source code, and rename it so the folder contains no version information (like Chart.js-1.0.2 becomes Chart.js). You should now have sites/*/libraries/Chart.js/Chart.min.js.
