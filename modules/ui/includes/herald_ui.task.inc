<?php

/**
 * @file
 * Task rendering.
 */

/**
 * Task modal callback.
 */
function herald_ui_task_modal_page($js, $task) {
  if ($js) {
    ctools_include('modal');
    ctools_include('ajax');
  }

  return ctools_modal_render(
    herald_ui_task_modal_title($task),
    theme('herald_ui_task_details', array(
      'task' => $task,
    ))
  ) ;
}

/**
 * Theme callback for herald_ui_task_details.
 */
function theme_herald_ui_task_details($vars) {
  $vars['task']->herald_ui_bypass = TRUE;
  $task_type = $vars['task']->task_type;

  if (preg_match('/(^simpletest_|csslint|eslint|drupal_cs)/', $task_type)) {
    $renderable = module_invoke_all('herald_task_view', $task_type, $vars['task']);
    return render($renderable);
  }

  return '';
}

/**
 * Implements hook_herald_task_view().
 *
 * True implementation of hook_herald_task_view().
 */
function _herald_ui_herald_task_view($task_type, $task) {
  $lib = libraries_load('Chart.js');

  if (empty($lib['loaded'])) {
    drupal_set_message(t("Could not load the Chart.js library. Please check it was installed correctly."));
  }

  if (preg_match('/(^simpletest_|csslint|eslint|drupal_cs)/', $task_type)) {
    if (preg_match('/^simpletest_/', $task_type)) {
      $task_type = 'simpletest';
    }

    // Include the CTools tools that we need.
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();

    // And our own assets.
    herald_ui_add($task_type, 'js');
    herald_ui_add(NULL, 'css');

    // Add some colors.
    drupal_add_js(array(
      'herald_ui' => array(
        'colors' => array(
          'green' => array(
            'normal' => '#39D34D',
            'highlight' => '#40ED57',
          ),
          'blue' => array(
            'normal' => '#46BFBD',
            'highlight' => '#5AD3D1',
          ),
          'orange' => array(
            'normal' => '#FDB45C',
            'highlight' => '#FFC870',
          ),
          'red' => array(
            'normal' => '#F7464A',
            'highlight' => '#FF5A5E',
          ),
        ),
      ),
    ), 'setting');
  }
}
