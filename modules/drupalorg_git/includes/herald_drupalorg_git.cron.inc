<?php

/**
 * @file
 * Module cron logic.
 */

/**
 * Load all Drupal.org projects and create new builds.
 */
function herald_drupalorg_git_fetch_builds() {
  $time_ago = variable_get('herald_drupalorg_git_cron_build_time_ago', 6) * 3600;
  $limit = variable_get('herald_drupalorg_git_cron_build_limit', 20);

  if ($projects = herald_drupalorg_git_load_projects($time_ago, $limit)) {
    foreach ($projects as $project) {
      // Get the Git settings.
      $settings = herald_drupalorg_git_get_settings($project);

      $git_url = $settings['git_url'];
      $git_branch = $settings['git_branch'];
      $destination = herald_drupalorg_git_compute_build_location($project, $git_branch);
      $git_repos_path = file_stream_wrapper_get_instance_by_uri('git://')->realpath();
      $repo_path = $git_repos_path . '/' . str_replace('git://', '', $destination);

      // Did we clone it previously?
      if (file_exists("{$repo_path}/.git")) {
        // We check if there's any difference. If not, there's no point in
        // registering a build.
        $repo_path = escapeshellarg($repo_path);

        $result = trim(shell_exec("cd {$repo_path} && git fetch && git diff origin/{$git_branch} --summary"));

        if (empty($result)) {
          // No new code. Abort this build.
          watchdog('herald_drupalorg_git', "Project %title has no changes. Canceling build.", array(
            '%title' => $project->title,
          ), WATCHDOG_INFO, l(t("Go to project"), "node/{$project->nid}"));

          continue;
        }
      }

      if (herald_git_clone($git_url, $destination, NULL, $git_branch)) {
        // Register a new build.
        $date = date('Y/m/d H:i:s');
        $build = herald_register_build($project->nid, 'drupalorg_git', "{$git_branch} build, {$date}");

        // Update the built timestamp.
        herald_drupalorg_git_store_settings($project, $git_url, $git_branch, REQUEST_TIME);

        watchdog('herald_drupalorg_git', "Created new build for project !nid: %title.", array(
          '!nid' => $project->nid,
          '%title' => $build->title,
        ), WATCHDOG_INFO, l(t("Go to build"), "node/{$build->nid}"));
      }
      else {
        watchdog('herald_drupalorg_git', "Could not clone Git repo. Git URL: @url, destination: @dest, branch: @branch.", array(
          '@url' => $git_url,
          '@dest' => $destination,
          '@branch' => $git_branch,
        ), WATCHDOG_ERROR);
      }
    }
  }
}

/**
 * Load projects that use drupalorg_git builds.
 *
 * We get all projects that were built more than a certain time ago, starting
 * with the ones that were built the longest ago.
 *
 * @param int $time_ago
 *    How much time must have passed before attempting to build again, in
 *    seconds.
 * @param int $limit
 *    (optional) A maximum number of projects to load. Defaults to 50.
 *
 * @return array|false
 *    A list of projects, or false.
 */
function herald_drupalorg_git_load_projects($time_ago, $limit = 50) {
  $admin = user_load(1);
  $vids = array();
  $result = db_select('herald_drupalorg_git_project_settings', 'h')
            ->fields('h', array('vid'))
            ->condition('h.last_built', REQUEST_TIME - $time_ago, '<')
            ->execute();

  while ($vid = $result->fetchField()) {
    $vids[] = $vid;
  }

  if (empty($vids)) {
    return FALSE;
  }

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'herald_project')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->propertyCondition('vid', $vids, 'IN')
    ->fieldCondition('herald_build_types', 'value', 'drupalorg_git', '=')
    ->range(0, $limit)
    ->addMetaData('account', $admin);

  $result = $query->execute();

  if (isset($result['node'])) {
    $project_nids = array_keys($result['node']);
    return entity_load('node', $project_nids);
  }
  else {
    return FALSE;
  }
}
