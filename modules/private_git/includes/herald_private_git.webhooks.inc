<?php

/**
 * @file
 * Webhook callbacks.
 */

/**
 * Page callback for the webhook.
 */
function herald_private_git_webhook_callback($project) {
  $ref = isset($_GET['ref']) ? $_GET['ref'] : 'master';

  // Get the Git settings.
  $settings = herald_private_git_get_settings($project);

  $git_url = $settings['git_url'];
  // @todo support public/private key pairs.
  $private_key = FALSE;
  $destination = herald_private_git_compute_build_location($project, $ref);

  if (herald_git_clone($git_url, $destination, $ref, NULL, $private_key)) {
    // Register a new build.
    herald_register_build($project->nid, 'private_git', $ref);

    // Bypass page rendering.
    die('OK');
  }
  else {
    watchdog('herald_private_git', "Could not clone Git repo. Git URL: @url, destination: @dest, commit: @commit. Using private key: @key", array(
      '@url' => $git_url,
      '@dest' => $destination,
      '@commit' => $ref,
      '@key' => !empty($private_key) ? $private_key : 'none',
    ), WATCHDOG_ERROR);

    // Bypass page rendering.
    die('An error occurred');
  }
}
