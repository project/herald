<?php

/**
 * @file
 * Git file stream wrapper.
 */

class HeraldGitStreamWrapper extends DrupalLocalStreamWrapper {

  /**
   * {@inheritdoc}
   */
  public function getDirectoryPath() {
    return variable_get('herald_git_build_location', '/tmp');
  }

  /**
   * {@inheritdoc}
   *
   * The repos are not available for external URLs.
   */
  function getExternalUrl() {
    return 0;
  }
}
