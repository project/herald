<?php

/**
 * @file
 * Template for rendering a single task run result.
 *
 * Available variables:
 * - $file: The filename for this result, safe for printing.
 * - $result: The result.
 * - $task: The task node.
 */
?>
<div class="eslint-results__details__result">
  <h4 class="eslint-results__details__result__file-name eslint-results__details__result__file-name--<?php print !empty($result) ? 'error' : 'ok'; ?>">
    <?php print $file; ?>
  </h4>

  <?php if (!empty($result)): ?>
    <div class="eslint-results__details__result__error">
      <pre><code>
        <?php print check_plain($result); ?>
      </code></pre>
    </div>
  <?php endif; ?>
</div>
