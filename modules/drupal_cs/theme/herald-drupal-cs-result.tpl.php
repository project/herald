<?php

/**
 * @file
 * Template for rendering a single task run result.
 *
 * Available variables:
 * - $file: The filename for this result, safe for printing.
 * - $result: The result, rendered and safe for printing.
 * - $task: The task node.
 * - $stats: The stats for the file.
 */
$error_type =
  !empty($stats['critical']) ? 'critical' :
    (!empty($stats['normal']) ? 'normal' :
      (!empty($stats['minor']) ? 'minor' : 'ok'));
?>
<div class="drupal-cs-results__details__result">
  <h4 class="drupal-cs-results__details__result__file-name drupal-cs-results__details__result__file-name--<?php print $error_type; ?>">
    <?php print $file; ?>
  </h4>

  <div class="drupal-cs-results__details__result__error drupal-cs-results__details__result__error--<?php print $error_type; ?>">
    <?php print $result; ?>
  </div>
</div>
