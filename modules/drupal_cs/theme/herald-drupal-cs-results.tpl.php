<?php

/**
 * @file
 * Drupal code standards task result rendering.
 *
 * Available variables:
 * - $stats: An array of statistics, globally.
 * - $file_stats: An array of statistics, per file.
 * - $results: Array of results, already rendered and safe for printing.
 * - $task: The task node.
 */
?>
<div class="drupal-cs-results">
  <div class="drupal-cs-results__grade">
    <h3><?php print t("Grade for this build: !grade", array('!grade' => call_user_func_array('herald_drupal_cs_compute_grade', $file_stats))); ?></h3>
  </div>

  <div class="drupal-cs-results__stats">
    <h3><?php print t("Statistics"); ?></h3>

    <?php print theme('table', array(
      'header' => array(t("Total number of files"), t("Files without errors"), t("Files with minor issues"), t("Files with normal issues"), t("Files with critical issues")),
      'rows' => array($file_stats),
    )); ?>

    <?php print theme('table', array(
      'header' => array(t("Minor issues"), t("Normal issues"), t("Critical issues"), t("Ignored issues")),
      'rows' => array($stats),
    )); ?>
  </div>

  <?php if (!empty($results)): ?>
    <div class="drupal-cs-results__details">
      <h3><?php print t("Details"); ?></h3>

      <?php foreach ($results as $result): ?>
        <?php print $result; ?>
      <?php endforeach; ?>
    </div>
  <?php else: ?>
    <div class="drupal-cs-results__no-issues">
      <h3><?php print t("Congratulations! No issues were found!"); ?></h3>
    </div>
  <?php endif; ?>
</div>
