<?php

/*
 * This is another Drupal CS test file. It does NOT respect Drupal coding
 * standards, on purpose. It should be ignored during test runs, to test file
 * exclusion.
 */

/*
 * implement hook_menu
 */
function herald_drupal_cs_test_menu()
{
  return array(
    'herald-drupal-cs-test-path' => array(
      'page callback' => 'phpinfo',
    ),
  );
}
