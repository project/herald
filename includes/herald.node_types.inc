<?php

/**
 * @file
 * Helpers for node type installing.
 */

/**
 * Implements hook_node_type_insert().
 *
 * Actual implementation of hook_node_type_insert().
 */
function _herald_node_type_insert($content_type) {
  if ($content_type->type == 'herald_project') {
    // Create all the fields we are adding to our content type.
    foreach (_herald_project_content_type_fields() as $field) {
      field_create_field($field);
    }

    // Create all the instances for our fields.
    foreach (_herald_project_content_type_field_instances() as $instance) {
      field_create_instance($instance);
    }
  }

  if ($content_type->type == 'herald_build') {
    // Create all the fields we are adding to our content type.
    foreach (_herald_build_content_type_fields() as $field) {
      field_create_field($field);
    }

    // Create all the instances for our fields.
    foreach (_herald_build_content_type_field_instances() as $instance) {
      field_create_instance($instance);
    }
  }

  if ($content_type->type == 'herald_task') {
    // Create all the fields we are adding to our content type.
    foreach (_herald_task_content_type_fields() as $field) {
      field_create_field($field);
    }

    // Create all the instances for our fields.
    foreach (_herald_task_content_type_field_instances() as $instance) {
      field_create_instance($instance);
    }
  }
}

/**
 * Defines the fields for the herald_project content type.
 */
function _herald_project_content_type_fields() {
  return array(
    'herald_project_core_version' => array(
      'field_name' => 'herald_project_core_version',
      'cardinality' => 1,
      'type' => 'list_text',
      'settings' => array(
        'allowed_values' => array(
          '6.x' => '6.x',
          '7.x' => '7.x',
          '8.x' => '8.x',
        ),
      ),
    ),
    'herald_build_types' => array(
      'field_name' => 'herald_build_types',
      'cardinality' => -1,
      'type' => 'list_text',
      'settings' => array(
        'allowed_values_function' => 'herald_get_build_types',
      ),
    ),
    'herald_task_types' => array(
      'field_name' => 'herald_task_types',
      'cardinality' => -1,
      'type' => 'list_text',
      'settings' => array(
        'allowed_values_function' => 'herald_get_task_types',
      ),
    ),
  );
}

/**
 * Defines the field instances for the herald_project content type.
 */
function _herald_project_content_type_field_instances() {
  return array(
    'herald_project_core_version' => array(
      'entity_type' => 'node',
      'bundle' => 'herald_project',
      'field_name' => 'herald_project_core_version',
      'label' => t("Core version"),
      'required' => TRUE,
      'widget' => array(
        'type' => 'options_buttons',
      ),
      'default_value' => array(
        0 => array(
          '7.x',
        ),
      ),
    ),
    'herald_build_types' => array(
      'entity_type' => 'node',
      'bundle' => 'herald_project',
      'field_name' => 'herald_build_types',
      'label' => t("Build types"),
      'required' => TRUE,
      'widget' => array(
        'type' => 'options_buttons',
      ),
    ),
    'herald_task_types' => array(
      'entity_type' => 'node',
      'bundle' => 'herald_project',
      'field_name' => 'herald_task_types',
      'label' => t("Task types"),
      'required' => TRUE,
      'widget' => array(
        'type' => 'options_buttons',
      ),
    ),
  );
}

/**
 * Defines the fields for the herald_build content type.
 */
function _herald_build_content_type_fields() {
  return array(
    'herald_project_ref' => array(
      'field_name' => 'herald_project_ref',
      'cardinality' => 1,
      'type' => 'entityreference',
      'settings' => array(
        'target_type' => 'node',
        'handler_settings' => array(
          'target_bundles' => array('herald_project'),
        ),
      ),
    ),
    'herald_build_type' => array(
      'field_name' => 'herald_build_type',
      'cardinality' => 1,
      'type' => 'list_text',
      'settings' => array(
        'allowed_values_function' => 'herald_get_build_types',
      ),
    ),
  );
}

/**
 * Defines the field instances for the herald_build content type.
 */
function _herald_build_content_type_field_instances() {
  return array(
    'herald_project_ref' => array(
      'entity_type' => 'node',
      'bundle' => 'herald_build',
      'field_name' => 'herald_project_ref',
      'label' => t("Project"),
      'required' => TRUE,
      'widget' => array(
        'type' => 'entityreference_autocomplete',
      ),
    ),
    'herald_build_type' => array(
      'entity_type' => 'node',
      'bundle' => 'herald_build',
      'field_name' => 'herald_build_type',
      'label' => t("Build type"),
      'required' => TRUE,
      'widget' => array(
        'type' => 'options_buttons',
      ),
    ),
  );
}

/**
 * Defines the fields for the herald_task content type.
 */
function _herald_task_content_type_fields() {
  return array(
    'herald_build_ref' => array(
      'field_name' => 'herald_build_ref',
      'cardinality' => 1,
      'type' => 'entityreference',
      'settings' => array(
        'target_type' => 'node',
        'handler_settings' => array(
          'target_bundles' => array('herald_build'),
        ),
      ),
    ),
    'herald_task_type' => array(
      'field_name' => 'herald_task_type',
      'cardinality' => 1,
      'type' => 'list_text',
      'settings' => array(
        'allowed_values_function' => 'herald_get_task_types',
      ),
    ),
    'herald_task_status' => array(
      'field_name' => 'herald_task_status',
      'cardinality' => 1,
      'type' => 'list_text',
      'settings' => array(
        'allowed_values' => array(
          'queued' => "Queued",
          'running' => "Running",
          'finished' => "Finished",
        ),
      ),
    ),
  );
}

/**
 * Defines the field instances for the herald_task content type.
 */
function _herald_task_content_type_field_instances() {
  return array(
    'herald_build_ref' => array(
      'entity_type' => 'node',
      'bundle' => 'herald_task',
      'field_name' => 'herald_build_ref',
      'label' => t("Project build"),
      'required' => TRUE,
      'widget' => array(
        'type' => 'entityreference_autocomplete',
      ),
    ),
    'herald_task_type' => array(
      'entity_type' => 'node',
      'bundle' => 'herald_task',
      'field_name' => 'herald_task_type',
      'label' => t("Task type"),
      'required' => TRUE,
    ),
    'herald_task_status' => array(
      'entity_type' => 'node',
      'bundle' => 'herald_task',
      'field_name' => 'herald_task_status',
      'label' => t("Task status"),
      'required' => TRUE,
      'widget' => array(
        'type' => 'options_buttons',
      ),
      'default_value' => array(
        0 => array(
          'queued',
        ),
      ),
    ),
  );
}
