<?php

/**
 * @file
 * Callbacks and hooks for node forms.
 */

/**
 * Add the title field to our node forms.
 */
function _herald_add_content_type_title_field($node, $form) {
  $type = node_type_get_type($node);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#default_value' => !empty($node->title) ? $node->title : '',
    '#required' => TRUE,
    '#weight' => -5
  );

  return $form;
}

/**
 * Actual validation callback for herald_project_form().
 */
function _herald_project_form_validate($form, $form_state, $hook = 'validate') {
  $hook_type = "#herald_{$hook}_callback";

  foreach (module_invoke_all('herald_project_settings', $form_state['node']) as $settings_key => $settings_info) {
    // Do we have a callback?
    if (!empty($form[$settings_key][$hook_type]) && function_exists($form[$settings_key][$hook_type])) {
      // We have a callback.
      if (!empty($settings_info['build_types'])) {
        // If the build types don't apply to this project, we don't call it.
        $call = FALSE;
        foreach ($form_state['values']['herald_build_types'][LANGUAGE_NONE] as $key => $value) {
          if (in_array($value['value'], $settings_info['build_types'])) {
            $call = TRUE;
            break;
          }
        }
      }
      elseif (!empty($settings_info['task_types'])) {
        // If the task types don't apply to this project, we don't call it.
        $call = FALSE;
        foreach ($form_state['values']['herald_task_types'][LANGUAGE_NONE] as $key => $value) {
          if (in_array($value['value'], $settings_info['task_types'])) {
            $call = TRUE;
            break;
          }
        }
      }
      else {
        // It applies to any build or task type. Call the callbacks.
        $call = TRUE;
      }

      if ($call) {
        call_user_func_array($form[$settings_key][$hook_type], array(
          $form_state['node'],
          array('values' => isset($form_state['values'][$settings_key]) ? $form_state['values'][$settings_key] : array()),
        ));
      }
    }
  }
}

/**
 * Helper function to add fields to a project.
 *
 * Modules can add fields for task or build options to the node form. This
 * function helps to add these fields, as well as to register validation and
 * submission callbacks.
 *
 * @param array $form
 *    The node form.
 * @param array $list
 *    The list of results of invoking hook_herald_project_settings().
 * @param int $weight
 *    (optional) The default weight. Each fieldset weight will be incremented
 *    by 1. Defaults to 10.
 *
 * @return array
 *    An update form array.
 */
function _herald_add_build_and_task_settings_to_form($form, $list, $weight = 10) {
  foreach ($list as $settings_key => $settings_info) {
    $form[$settings_key] = array(
      '#type' => 'fieldset',
      '#title' => !empty($settings_info['title']) ? $settings_info['title'] : t("Settings for @key", array('@key' => $settings_key)),
      '#tree' => TRUE,
      '#weight' => $weight++,
      '#collapsible' => TRUE,
      '#collapsed' => !empty($form['#project']->nid),
      '#herald_submit_callback' => !empty($settings_info['callbacks']['submit']) ? $settings_info['callbacks']['submit'] : "{$settings_key}_submit",
      '#herald_validate_callback' => !empty($settings_info['callbacks']['validate']) ? $settings_info['callbacks']['validate'] : "{$settings_key}_validate",
    );

    // Does it only apply to certain build types? If so, we hide it if the build
    // types are not checked.
    if (!empty($settings_info['build_types'])) {
      $form[$settings_key]['#states'] = array('invisible' => array());
      foreach ($settings_info['build_types'] as $build_type) {
        $input_name = ':input[name="herald_build_types[und][' . $build_type . ']"]';
        $form[$settings_key]['#states']['invisible'][$input_name] = array('checked' => FALSE);
      }
    }

    // Does it only apply to certain task types? If so, we hide it if the task
    // types are not checked.
    if (!empty($settings_info['task_types'])) {
      $form[$settings_key]['#states'] = array('invisible' => array());
      foreach ($settings_info['task_types'] as $task_type) {
        $input_name = ':input[name="herald_task_types[und][' . $task_type . ']"]';
        $form[$settings_key]['#states']['invisible'][$input_name] = array('checked' => FALSE);
      }
    }

    foreach ($settings_info['form'] as $key => $form_item) {
      $form[$settings_key][$key] = $form_item;
    }
  }

  return $form;
}
