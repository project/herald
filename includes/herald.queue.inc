<?php

/**
 * @file
 * Module queue logic.
 */

/**
 * Run queued tasks
 *
 * Run a fixed amount of pending tasks. We fetch pending tasks per task type,
 * because it is possible to set different limits for the amount of tasks to
 * start per type. Running test suites takes much longer than checking code
 * style, for example, so we might want to run less of the former than the
 * latter.
 *
 * If the background_process module exists and is enabled, we will use it to
 * run our tasks. Otherwise, we use the Drupal Queue API.
 */
function herald_process_task_queue() {
  $background_process_available = module_exists('background_process') && function_exists('background_process_start');

  if (!$background_process_available) {
    $queue = DrupalQueue::get('herald_task_runner');
  }

  foreach (array_keys(herald_get_task_types()) as $task_type) {
    // Compare the currently running tasks to the maximum we can run.
    $running = variable_get("herald_running_{$task_type}_tasks", 0);
    $max = variable_get("herald_max_{$task_type}_tasks", 5);

    // We can run this many tasks of this type right now.
    $limit = $max - $running;

    if ($limit > 0) {
      $tasks = herald_load_queued_tasks($task_type, $limit);

      if (!empty($tasks)) {
        foreach ($tasks as $task) {
          if ($background_process_available) {
            // We can use background processes! Start the process now, instead
            // of queuing them up; there's no point in waiting any longer.
            background_process_start('herald_task_runner', $task);
          }
          else {
            // Queue them up again, this time against the Drupal Queue API. This
            // is slower and requires more cron runs, but at least works out
            // of the box.
            $queue->createItem($task);
          }

          watchdog('herald', "Queued task for execution. Task: %title.", array(
            '%title' => $task->title,
          ), WATCHDOG_INFO, l(t("Go to task"), "node/{$task->nid}"));
        }
      }
    }
  }
}

/**
 * Load queued tasks.
 *
 * Load all queued tasks of a specific task type.
 *
 * @param string $task_type
 *    The task type to load queued tasks for.
 * @param int $limit
 *    (optional) A maximum number of tasks to load. Defaults to 50.
 *
 * @return array|false
 *    A list of queued tasks, or false.
 */
function herald_load_queued_tasks($task_type, $limit = 50) {
  $admin = user_load(1);

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'herald_task')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldCondition('herald_task_status', 'value', 'queued', '=')
    ->fieldCondition('herald_task_type', 'value', $task_type, '=')
    ->propertyOrderBy('created', 'ASC')
    ->range(0, $limit)
    ->addMetaData('account', $admin);

  $result = $query->execute();

  if (isset($result['node'])) {
    $task_nids = array_keys($result['node']);
    return entity_load('node', $task_nids);
  }
  else {
    return FALSE;
  }
}
