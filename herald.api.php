<?php

/**
 * @file
 * Module API documentation.
 */

/**
 * Implements hook_herald_build_type().
 *
 * Define a build type. Build types can allow modules to intervene in a specific
 * manner when builds are being manipulated or created. When a new project is
 * created, it can specify which types of builds can be generated for it.
 *
 * @return array
 *    An array of build types, keyed by machine name, and where the value is the
 *    human readable name.
 */
function hook_herald_build_type() {
  return array(
    'private_git' => t("Private Git repository"),
  );
}

/**
 * Implements hook_herald_task_type().
 *
 * Define a task type. Task types can allow modules to intervene in a specific
 * manner on cron runs, task rendering, etc. When a new project is created, it
 * can specify which types of tasks should be run for each build.
 *
 * @return array
 *    An array of task types, keyed by machine name, and where the value is the
 *    human readable name.
 */
function hook_herald_task_type() {
  return array(
    'drupal_cs' => t("Drupal code standards"),
  );
}

/**
 * Implements hook_herald_build_registered().
 *
 * When a new build is registered, modules can intervene. A classic example is
 * when a module registers a task for the build. Tasks can only be registered if
 * the project was configured for them. This is checked silently, so modules can
 * simply try to register a task for every build; Herald will ignore them if
 * they do not apply.
 *
 * @param object $build
 *    The new build.
 * @param object $project
 *    The project the build was for.
 */
function hook_herald_build_registered($build, $project) {
  herald_register_task(
    $build->id,
    'drupal_cs',
    t("Check Drupal coding standards for @build", array('@build' => $build->title))
  );
}

/**
 * Implements hook_herald_build_alter().
 *
 * When a new build is registered, modules can alter it.
 *
 * @param object &$build
 *    The new build.
 * @param object $project
 *    The project the build was for.
 */
function hook_herald_build_alter(&$build, $project) {

}

/**
 * Implements hook_herald_task_registered().
 *
 * When a new task is registered, modules can intervene.
 *
 * @param object $task
 *    The new task.
 * @param object $build
 *    The build the task was for.
 * @param object $project
 *    The project the build was for.
 */
function hook_herald_task_registered($task, $build, $project) {

}

/**
 * Implements hook_herald_task_alter().
 *
 * When a new task is registered, modules can alter it.
 *
 * @param object &$task
 *    The new task.
 * @param object $build
 *    The build the task was for.
 */
function hook_herald_task_alter(&$task, $build) {

}

/**
 * Implements hook_herald_build_location().
 *
 * Allows modules to return the location for a build. The first module to
 * register the build and return something wins, and its result will be used.
 *
 * @param string $build_type
 *    The type of the build.
 * @param object $build
 *    The build being requested.
 * @param object $project
 *    The project the build is for.
 *
 * @return string
 *    The absolute path to the build code.
 */
function hook_herald_build_location($build_type, $build, $project) {
  if ($build_type == 'private_git') {
    $commit_hash = $build->title;
    return "/tmp/$commit_hash";
  }
}

/**
 * Implements hook_herald_build_location_alter().
 *
 * Alter the location returned by hook_herald_build_location().
 *
 * @param string &$location
 *    The location of the build.
 * @param object $build
 *    The build being requested.
 */
function hook_herald_build_location_alter(&$location, $build) {

}

/**
 * Implements hook_herald_run_task().
 *
 * Called whenever a task is run. Modules can choose to do something with the
 * build, or ignore it. If a module decides it is the sole owner of the task,
 * it should mark the task as finished at the end of the run, so as not to run
 * tasks twice.
 *
 * @param string $task_type
 *    The type of the task.
 * @param object $task
 *    The task object.
 * @param object $build
 *    The build the task is for.
 * @param object $project
 *    The project the build is for.
 */
function hook_herald_run_task($task_type, $task, $build, $project) {
  if ($task_type == 'drupal_cs') {
    // Get the build code location.
    $build_location = herald_get_build_location($build);

    // So something with the build...

    // Mark the task as terminated.
    herald_task_terminated($task);
  }
}

/**
 * Implements hook_herald_project_settings().
 *
 * Add settings to the project creation and editing form to add custom fields
 * for the build types. These settings values must be stored and validated by
 * the module that added them.
 *
 * @param object $project
 *    (optional) The project the settings are being loaded for. This can be
 *    a new project, so it may not be complete and have all its data yet.
 *
 * @return array
 *    An array of setting groups to add to the form when creating or editing
 *    a project. Each entry is another array, containing information on the
 *    settings. The key for each entry must be unique, and can be used to
 *    construct the validation and submission callbacks, unless these are
 *    specified (see below).
 *    - "form": Required. A form array, which will be included in a fieldset.
 *      This form can contain any fields required. The values of these fields
 *      will be passed to the submission and validation callbacks. It is highly
 *      recommended to NOT mark these fields as required, as you do not know
 *      if the project uses the build type associated with the fields.
 *    - "title": Optional. The title for the fieldset that will contain the
 *      the build settings.
 *    - "build_types": Optional. An array of build types these settings apply
 *      to. Herald will hide the fieldset if the project build type or types
 *      don't correspond, simplifying the form. This will also make sure the
 *      validation and submission callbacks are not triggered unnecessarily.
 *    - "task_types": Optional. An array of task types these settings apply
 *      to. Herald will hide the fieldset if the project task type or types
 *      don't correspond, simplifying the form. This will also make sure the
 *      validation and submission callbacks are not triggered unnecessarily.
 *    - "callbacks": Optional. An array of callbacks for validation and
 *      submission logic. If the "build_types" are specified (see above), these
 *      callbacks will only be triggered if the project uses one of the build
 *      types.
 *      - "submit": Optional. A submission callback. A callback will get the
 *        project node as the first parameter, and the provided field values as
 *        the second parameter. If the submit callback is not provided, Herald
 *        will see if a function exists with the name "{entry key}_submit". If
 *        no callback is found, it is simply ignored.
 *      - "validate": Optional. A validation callback. A callback will get the
 *        project node as the first parameter, and the provided field values as
 *        the second parameter. If the validate callback is not provided, Herald
 *        will see if a function exists with the name "{entry key}_validate". If
 *        no callback is found, it is simply ignored.
 */
function hook_herald_project_settings($project = NULL) {
  $form = array();

  // Prepare the form fields.
  $form['private_git_repository_url'] = array(
    '#type' => 'textfield',
    '#title' => t("Git repository URL"),
    '#default_value' => empty($project->id) ?: variable_get('private_git_repository_url_' . $project->id, ''),
  );

  // Return our settings array.
  return array(
    'private_git_settings' => array(
      'title' => t("Private Git repository settings"),
      // We only apply our settings to the 'private_git' build type.
      'build_types' => array('private_git'),
      'form' => $form,
      // We could omit this and use private_git_settings_validate and
      // private_git_settings_submit.
      'callbacks' => array(
        'submit' => 'private_git_project_build_settings_submit',
        'validate' => 'private_git_project_build_settings_validate',
      ),
    ),
  );
}

/**
 * Implements hook_herald_project_view().
 *
 * Add information to the project being rendered. This is similar to
 * hook_node_view().
 *
 * @see hook_node_view()
 *
 * @param object $project
 *    The project being rendered.
 * @param array $build_types
 *    An array of build types this project uses.
 * @param array $task_types
 *    An array of task types this project uses.
 *
 * @return array
 *    A renderable array with information about the project.
 */
function hook_herald_project_view($project, $build_types, $task_types) {

}

/**
 * Implements hook_herald_build_view().
 *
 * Add information to the build being rendered. This is similar to
 * hook_node_view().
 *
 * @see hook_node_view()
 *
 * @param string $build_type
 *    The type of the build being rendered.
 * @param object $build
 *    The build being rendered.
 *
 * @return array
 *    A renderable array with information about the build.
 */
function hook_herald_build_view($build_type, $build) {

}

/**
 * Implements hook_herald_task_view().
 *
 * Add information to the task being rendered. This is similar to
 * hook_node_view().
 *
 * @see hook_node_view()
 *
 * @param string $task_type
 *    The type of the task being rendered.
 * @param object $task
 *    The task being rendered.
 *
 * @return array
 *    A renderable array with information about the task. Information like task
 *    status and name will already be available, and don't need to be added.
 */
function hook_herald_task_view($task_type, $task) {

}
